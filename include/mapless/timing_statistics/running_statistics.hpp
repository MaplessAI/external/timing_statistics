/*
 * MIT License
 *
 * Copyright (c) 2020 Mapless AI, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef MAPLESS__RUNNING_STATISTICS__RUNNING_STATISTICS_HPP_
#define MAPLESS__RUNNING_STATISTICS__RUNNING_STATISTICS_HPP_

#include <string>

namespace mapless {

/** @brief Simple class for maintaining running statistics. */
class Running_Statistics {
 public:
  Running_Statistics();

  /** @brief Add a sample to the accumulators. */
  void update_stats(double sample);

  /** @brief Compute and return running statistics. */
  static std::string string(const Running_Statistics& rs);

 private:
  size_t sample_count;
  double last_sample;
  double M2;
  double mean;
  double min_value;
  double max_value;
};

}  // namespace mapless

#endif  // MAPLESS__RUNNING_STATISTICS__RUNNING_STATISTICS_HPP_
