# ROS Timing Statistics

This is a simple package that contains a scoped timer for quick-and-dirty timing of code blocks in ROS packages.

The actual code is minimal and should be easy to adapt to non-ROS use cases if needed.

# Scoped Timer

An object of the scoped timer class computes the number of clock ticks spent inside a specific scope.
The object records the time at instantiation and computes elapsed time at destruction.
The elapsed time is logged at `INFO` verbosity to the ROS logger.
Each `Scoped_Timer` instance can be constructed with a string name to make it easy to tell them apart if multiple timers are in use.

Timing a function body can be done by simply instantiating a `scoped_timer` at the top of the function:

```c++
"mapless/timing_statistics/scoped_timer.hpp"

void foo() {
  auto timer = mapless::Scoped_Timer("foo time");

  // do some work
}
```

Invoking the function will produce a log output like this:

```console
[INFO] [1611671354.246815567] [foo time]: 0.01ms
```

A block of code can be timed by wrapping the code in its own scope and instantiating a `scoped_timer` at the top:

```c++
"mapless/timing_statistics/scoped_timer.hpp"

void foo() {
  // do some work

  {
    auto timer = mapless::Scoped_Timer("inside foo time");

    // do some work that is timed
  }

  // do some other work
}
```

Invoking the function will produce a log output like this:

```console
[INFO] [1611671354.246815567] [inside foo time]: 0.005ms
```

# Timing Statistics

An object of class `Timing_Statistics` accumulates the results of `Scoped_Timer` objects over multiple instantiations and computes running statistics.
This can be useful for gathering insight on the run-time characteristics of callbacks, for example.

The intended way to use a `Timing_Statistics` object is to add it as a class member and then use its `get_timer` factory method to instantiate a `Scoped_Timer` inside the scope that is being timed.

In the below example, timing statistics are being computed for repeated invokations of the function `Foo::bar`:

```c++
#include "mapless/timing_statistics/scoped_timer.hpp"

class Foo : public rclcpp::Node {
  public:
  void bar() {
    auto timer = timing_stats_.get_timer("Cycle Time");

    // do some work
  }

  private:
  mapless::Timing_Statistics timing_stats_;
};
```

The first ten invokations of `Foo::bar` will log output like this:

```console
[INFO] [1611671351.046851869] [Cycle Time]: {last_sample: 0.839043ms, mean: 0.839043ms, variance: 0.000000, sample_variance: -nan, min_value: 0.839043, max_value: 0.839043, sample_count: 1}
[INFO] [1611671351.146378553] [Cycle Time]: {last_sample: 0.762146ms, mean: 0.800594ms, variance: 0.001478, sample_variance: 0.002957, min_value: 0.762146, max_value: 0.839043, sample_count: 2}
[INFO] [1611671351.245736149] [Cycle Time]: {last_sample: 0.179982ms, mean: 0.593724ms, variance: 0.086577, sample_variance: 0.129865, min_value: 0.179982, max_value: 0.839043, sample_count: 3}
[INFO] [1611671351.346147224] [Cycle Time]: {last_sample: 0.548821ms, mean: 0.582498ms, variance: 0.065311, sample_variance: 0.087081, min_value: 0.179982, max_value: 0.839043, sample_count: 4}
[INFO] [1611671351.446366166] [Cycle Time]: {last_sample: 0.746605ms, mean: 0.615319ms, variance: 0.056557, sample_variance: 0.070697, min_value: 0.179982, max_value: 0.839043, sample_count: 5}
[INFO] [1611671351.546389455] [Cycle Time]: {last_sample: 0.754248ms, mean: 0.638474ms, variance: 0.049812, sample_variance: 0.059774, min_value: 0.179982, max_value: 0.839043, sample_count: 6}
[INFO] [1611671351.646475246] [Cycle Time]: {last_sample: 0.825222ms, mean: 0.665152ms, variance: 0.046966, sample_variance: 0.054794, min_value: 0.179982, max_value: 0.839043, sample_count: 7}
[INFO] [1611671351.746398826] [Cycle Time]: {last_sample: 0.753309ms, mean: 0.676172ms, variance: 0.041945, sample_variance: 0.047938, min_value: 0.179982, max_value: 0.839043, sample_count: 8}
[INFO] [1611671351.846448038] [Cycle Time]: {last_sample: 0.798291ms, mean: 0.689741ms, variance: 0.038758, sample_variance: 0.043603, min_value: 0.179982, max_value: 0.839043, sample_count: 9}
[INFO] [1611671351.946456577] [Cycle Time]: {last_sample: 0.804667ms, mean: 0.701233ms, variance: 0.036071, sample_variance: 0.040079, min_value: 0.179982, max_value: 0.839043, sample_count: 10}
```
