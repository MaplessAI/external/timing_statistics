/*
 * MIT License
 *
 * Copyright (c) 2020 Mapless AI, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <algorithm>
#include <limits>
#include <string>

#include "mapless/timing_statistics/scoped_timer.hpp"

static constexpr auto NaN = std::numeric_limits<double>::quiet_NaN();
static constexpr auto INF = std::numeric_limits<double>::infinity();

namespace mapless {

//------------------------------------------------------------------------------

Running_Statistics::Running_Statistics()
    : sample_count(0), M2(0.0), mean(NaN), min_value(INF), max_value(-INF) {}

//------------------------------------------------------------------------------

std::string Running_Statistics::string(const Running_Statistics& rs) {
  const auto variance = rs.M2 / static_cast<double>(rs.sample_count);
  const auto sample_variance = rs.M2 / static_cast<double>(rs.sample_count - 1);

  const auto ls = rs.last_sample * 1e-6;
  const auto m = rs.mean * 1e-6;
  const auto v = variance * 1e-12;
  const auto sv = sample_variance * 1e-12;
  const auto min = rs.min_value * 1e-6;
  const auto max = rs.max_value * 1e-6;
  return "{last_sample: " + std::to_string(ls) + "ms" +
         ", mean: " + std::to_string(m) + "ms, variance: " + std::to_string(v) +
         ", sample_variance: " + std::to_string(sv) +
         ", min_value: " + std::to_string(min) +
         ", max_value: " + std::to_string(max) +
         ", sample_count: " + std::to_string(rs.sample_count) + "}";
}

//------------------------------------------------------------------------------

void Running_Statistics::update_stats(double sample) {
  ++sample_count;
  last_sample = sample;
  min_value = std::min(min_value, sample);
  max_value = std::max(max_value, sample);
  if (sample_count == 1) {
    mean = sample;
    return;
  }

  const auto delta = (sample - mean);
  mean += delta / static_cast<double>(sample_count);
  const auto delta2 = (sample - mean);
  M2 += (delta * delta2);
}

//------------------------------------------------------------------------------

}  // namespace mapless
