/*
 * MIT License
 *
 * Copyright (c) 2020 Mapless AI, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "mapless/timing_statistics/scoped_timer.hpp"

#include <ros/console.h>

#include <string>
#include <utility>

namespace mapless {

//------------------------------------------------------------------------------

Scoped_Timer Timing_Statistics::get_timer(std::string prefix) {
  return Scoped_Timer(std::move(prefix), this);
}

//------------------------------------------------------------------------------

Scoped_Timer::Scoped_Timer(std::string prefix, Timing_Statistics* ptr)
    : prefix(std::move(prefix)), ptr(ptr) {
  t1 = std::chrono::steady_clock::now();
}

//------------------------------------------------------------------------------

Scoped_Timer::Scoped_Timer(std::string prefix)
    : Scoped_Timer(std::move(prefix), nullptr) {}

//------------------------------------------------------------------------------

Scoped_Timer::Scoped_Timer() : Scoped_Timer("") {}

//------------------------------------------------------------------------------

Scoped_Timer::~Scoped_Timer() {
  const auto t2 = std::chrono::steady_clock::now();
  const auto nano_count =
      std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
  const auto nano = static_cast<double>(nano_count);
  if (ptr) {
    ptr->update_stats(nano);
    ROS_INFO_STREAM(prefix << Running_Statistics::string(*ptr));
  } else {
    const auto ms = nano * 1e-6;
    ROS_INFO_STREAM(prefix << std::to_string(ms) << "ms");
  }
}

//------------------------------------------------------------------------------

}  // namespace mapless
